package draugo.dev.evetools.items;


import draugo.dev.evetools.ItemType;
import draugo.dev.evetools.Items;
import draugo.dev.evetools.Name;

import java.util.HashMap;
import java.util.Map;

public final class Mineral extends Item {
    private static Map<Name, Mineral> minerals = new HashMap<>();
    private static Name[] mineralNames = {
            Name.TRITANIUM,
            Name.PYERITE,
            Name.MEXALLON,
            Name.ISOGEN,
            Name.NOCXIUM,
            Name.ZYDRINE,
            Name.MEGACYTE,
            Name.MORPHITE
    };

    static {
        for(Name name : mineralNames) {
            minerals.put(name, new Mineral(name));
        }
        for(Mineral mineral : minerals.values()) {
            Items.items.put(mineral.getName(), mineral);
        }
    }

    public static Mineral get(Name name) {
        return minerals.get(name);
    }

    private Mineral(Name name) {
        super(ItemType.MINERAL, name);
    }
}
