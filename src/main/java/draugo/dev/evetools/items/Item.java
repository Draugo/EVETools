package draugo.dev.evetools.items;

import draugo.dev.evetools.ItemType;
import draugo.dev.evetools.Name;

public abstract class Item {
    private final ItemType type;
    private final Name name;
    private final int itemId;

    protected Item(ItemType type, Name name) {
        this.type = type;
        this.name = name;
        this.itemId = name.getTypeId();
    }

    public ItemType getType() {
        return type;
    }

    public Name getName() {
        return name;
    }

    public int getItemId() {
        return itemId;
    }
}
