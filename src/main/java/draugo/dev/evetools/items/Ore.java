package draugo.dev.evetools.items;

import draugo.dev.evetools.ItemType;
import draugo.dev.evetools.Items;
import draugo.dev.evetools.Name;
import draugo.dev.evetools.Skill;

import java.util.HashMap;
import java.util.Map;

public final class Ore extends Item {
    private static final Map<Name, Ore> ores = new HashMap<>();

    public static Ore get(Name name) {
        return ores.get(name);
    }

    private static Name[] oreNames = {
            Name.VELDSPAR,
            Name.VELDSPAR_CONCENTRATED,
            Name.VELDSPAR_DENSE,

            Name.SCORDITE,
            Name.SCORDITE_CONDENSED,
            Name.SCORDITE_MASSIVE,

            Name.PYROXERES,
            Name.PYROXERES_SOLID,
            Name.PYROXERES_VISCOUS,

            Name.PLAGIOCLASE,
            Name.PLAGIOCLASE_AZURE,
            Name.PLAGIOCLASE_RICH,

            Name.OMBER,
            Name.OMBER_SILVERY,
            Name.OMBER_GOLDEN,

            Name.KERNITE,
            Name.KERNITE_FIERY,
            Name.KERNITE_LUMINOUS,

            Name.JASPET,
            Name.JASPET_PRISTINE,
            Name.JASPET_PURE,

            Name.HEMORPHITE,
            Name.HEMORPHITE_RADIANT,
            Name.HEMORPHITE_VIVID,

            Name.MERCOXIT,
            Name.MERCOXIT_MAGMA,
            Name.MERCOXIT_VITREOUS,

            Name.HEDBERGITE,
            Name.HEDBERGITE_GLAZED,
            Name.HEDBERGITE_VITRIC,

            Name.GNEISS,
            Name.GNEISS_IRIDESCENT,
            Name.GNEISS_PRISMATIC,

            Name.OCHRE_DARK,
            Name.OCHRE_OBSIDIAN,
            Name.OCHRE_ONYX,

            Name.CROKITE,
            Name.CROKITE_CRYSTALLINE,
            Name.CROKITE_SHARP,

            Name.BISTOT,
            Name.BISTOT_MONOCLINIC,
            Name.BISTOT_TRICLINIC,

            Name.SPODUMAIN,
            Name.SPODUMAIN_BRIGHT,
            Name.SPODUMAIN_GLEAMING,

            Name.ARKONOR,
            Name.ARKONOR_CRIMSON,
            Name.ARKONOR_PRIME
    };

    static {
        for(Name name : oreNames) {
            ores.put(name, new Ore(name));
        }
        for(Ore ore : ores.values()) {
            Items.items.put(ore.getName(), ore);
        }
    }

    private final Skill processingSkill;

    private Ore(Name name) {
        super(ItemType.ORE, name);
        this.processingSkill = Skill.getReprocessingSkill(name);
    }

    public Skill getProcessingSkill() {
        return processingSkill;
    }
}
