package draugo.dev.evetools;

public enum Name {
    // Mineral names
    TRITANIUM("Tritanium", 34),
    PYERITE("Pyerite", 35),
    MEXALLON("Mexallon", 36),
    ISOGEN("Isogen", 37),
    NOCXIUM("Nocxium", 38),
    ZYDRINE("Zydrine", 39),
    MEGACYTE("Megacyte", 50),
    MORPHITE("Morphite", 11399),

    // Ore names
    VELDSPAR("Veldspar", 1230),
    VELDSPAR_CONCENTRATED("Concentrated Veldspar", 17470),
    VELDSPAR_DENSE("Dense Veldspar", 17471),

    SPODUMAIN("Spodumain", 19),
    SPODUMAIN_BRIGHT("Bright Spodumain", 17466),
    SPODUMAIN_GLEAMING("Gleaming Spodumain", 17467),

    SCORDITE("Scordite", 1228),
    SCORDITE_CONDENSED("Condensed Scordite", 17463),
    SCORDITE_MASSIVE("Massive Scordite", 17464),

    PYROXERES("Pyroxeres", 1224),
    PYROXERES_SOLID("Solid Pyroxeres", 17459),
    PYROXERES_VISCOUS("Viscous Pyroxeres", 17460),

    PLAGIOCLASE("Plagioclase", 18),
    PLAGIOCLASE_AZURE("Azure Plagioclase", 17455),
    PLAGIOCLASE_RICH("Rich Plagioclase", 17456),

    OMBER("Omber", 1227),
    OMBER_SILVERY("Silvery Omber", 17867),
    OMBER_GOLDEN("Golden Omber", 17868),

    MERCOXIT("Mercoxit", 11396),
    MERCOXIT_MAGMA("Magma Mercoxit", 17869),
    MERCOXIT_VITREOUS("Vitreous Mercoxit", 17870),

    KERNITE("Kernite", 20),
    KERNITE_FIERY("Fiery Kernite", 17453),
    KERNITE_LUMINOUS("Luminous Kernite", 17452),

    JASPET("Jaspet", 1226),
    JASPET_PRISTINE("Pristine Jaspet", 17449),
    JASPET_PURE("Pure Jaspet", 17448),

    HEMORPHITE("Hemorphite", 1231),
    HEMORPHITE_RADIANT("Radiant Hemorphite", 17445),
    HEMORPHITE_VIVID("Vivid Hemorphite", 17444),

    HEDBERGITE("Hedbergite", 21),
    HEDBERGITE_GLAZED("Glazed Hedbergite", 17441),
    HEDBERGITE_VITRIC("Vitric Hedbergite", 17440),

    GNEISS("Gneiss", 1229),
    GNEISS_IRIDESCENT("Iridescent Gneiss", 17865),
    GNEISS_PRISMATIC("Prismatic Gneiss", 17866),

    OCHRE_DARK("Dark Ochre", 1232),
    OCHRE_OBSIDIAN("Obsidian Ochre", 17437),
    OCHRE_ONYX("Onyx Ochre", 17436),

    CROKITE("Crokite", 1225),
    CROKITE_CRYSTALLINE("Crystalline Crokite", 17433),
    CROKITE_SHARP("Sharp Crokite", 17432),

    BISTOT("Bistot", 1223),
    BISTOT_MONOCLINIC("Monoclinic Bistot", 17429),
    BISTOT_TRICLINIC("Triclinic Bistot", 17428),

    ARKONOR("Arkonor", 22),
    ARKONOR_CRIMSON("Crimson Arkonor", 17425),
    ARKONOR_PRIME("Prime Arkonor", 17426);

    private final String name;
    private final int typeId;

    Name(String name, int typeId) {
        this.name = name;
        this.typeId = typeId;
    }

    public String getName() {
        return name;
    }

    public int getTypeId() {
        return typeId;
    }
}
