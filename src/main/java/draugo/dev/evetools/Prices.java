package draugo.dev.evetools;

import com.google.common.collect.Lists;
import draugo.dev.evetools.marketdata.Query;
import draugo.dev.evetools.marketdata.data.MarketData;
import draugo.dev.evetools.marketdata.data.Result;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * For now contains hard coded prices for different things.
 * These should be fetched from some online resource at some point.
 */
public final class Prices {


    private static Map<Name, Float> buyPrices = new HashMap<>();
    private static Map<Name, Float> sellPrices = new HashMap<>();

    // TODO: Add current position and distance limit
    // For now just look for data in Forge (which will mostly be Jita)
    public static float highestBuyPrice(Name name, List<Region> regions) {
        if(buyPrices.containsKey(name)) {
            return buyPrices.get(name);
        }
        MarketData data = Query.buyOrders(Lists.asList(name, new Name[0]), regions);

        Float price = null;
        for(Result row : data.getEmd().getResult()) {
            if(price == null) {
                price = row.getRow().getPrice();
            } else if(row.getRow().getPrice() > price) {
                price = row.getRow().getPrice();
            }
        }
        price = (((int)(price*100))*1.0f)/100;
        buyPrices.put(name, price);
        return price;
    }

    // TODO: Add current position and distance limit
    // For now just look for data in Forge (which will mostly be Jita)
    public static float lowestSellPrice(Name name, List<Region> regions) {
        if(sellPrices.containsKey(name)) {
            return sellPrices.get(name);
        }
        MarketData data = Query.buyOrders(Lists.asList(name, new Name[0]), regions);

        Float price = null;
        for(Result row : data.getEmd().getResult()) {
            if(price == null) {
                price = row.getRow().getPrice();
            } else if(row.getRow().getPrice() < price) {
                price = row.getRow().getPrice();
            }
        }

        price = (((int)(price*100))*1.0f)/100;
        sellPrices.put(name, price);
        return price;
    }

    private Prices() {}
}
