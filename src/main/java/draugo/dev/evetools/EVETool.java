package draugo.dev.evetools;

import draugo.dev.evetools.items.Ore;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.HashMap;
import java.util.Map;

public class EVETool {

    public static final float BASE_YIELD = 0.50f;

    public static final float REPROCESS_IMPLANT = 0.00f;

    private static final Map<Skill, Integer> skills = new HashMap<>();

    static {
        skills.put(Skill.HEMORPHITE_PROCESSING, 3);
        skills.put(Skill.JASPET_PROCESSING, 3);
        skills.put(Skill.KERNITE_PROCESSING, 3);
        skills.put(Skill.OMBER_PROCESSING, 3);
        skills.put(Skill.PLAGIOCLASE_PROCESSING, 4);
        skills.put(Skill.PYROXERES_PROCESSING, 4);
        skills.put(Skill.REPROCESSING, 5);
        skills.put(Skill.REPROCESSING_EFFICIENCY, 5);
        skills.put(Skill.SCORDITE_PROCESSING, 4);
        skills.put(Skill.VELDSPAR_PROCESSING, 4);
    }

    public static int getSkill(Skill skill) {
        return skills.get(skill);
    }

    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringConfig.class);




        EVETool instance = new EVETool();
        instance.runOreComparison(Ore.get(Name.HEMORPHITE_RADIANT), 100000);
        /*for(Name name : Name.values()) {
            System.out.println("prices.put(Name."+name.name()+", 0f);");
        }*/
    }

    public void runOreComparison(Ore ore, int amount) {
        OreComparer comparer = new OreComparer(ore, amount);
        comparer.printInfo();

        comparer.runComparison();
    }
}
