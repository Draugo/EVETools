package draugo.dev.evetools.marketdata.data;

public class MarketData {
    private EMD emd;

    public EMD getEmd() {
        return emd;
    }

    public void setEmd(EMD emd) {
        this.emd = emd;
    }
}
