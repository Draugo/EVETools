package draugo.dev.evetools.marketdata.data;

import java.util.ArrayList;
import java.util.List;

public class EMD {
    private int version;
    private String currentTime;
    private String name;
    private String key;
    private String columns;
    private final List<Result> result = new ArrayList<>();

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getColumns() {
        return columns;
    }

    public void setColumns(String columns) {
        this.columns = columns;
    }

    public List<Result> getResult() {
        return result;
    }
}
