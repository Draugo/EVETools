package draugo.dev.evetools.marketdata.data;

public class Result {
    private Row row;

    public Row getRow() {
        return row;
    }

    public void setRow(Row row) {
        this.row = row;
    }
}
