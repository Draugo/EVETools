package draugo.dev.evetools.marketdata.data;

public class Row {
    private String buysell;
    private long typeID;
    private long orderID;
    private long stationID;
    private long solarsystemID;
    private long regionID;
    private float price;
    private long volEntered;
    private long volRemaining;
    private long minVolume;
    private int range;
    private String issued;
    private String expires;
    private String created;

    public String getBuysell() {
        return buysell;
    }

    public void setBuysell(String buysell) {
        this.buysell = buysell;
    }

    public long getTypeID() {
        return typeID;
    }

    public void setTypeID(long typeID) {
        this.typeID = typeID;
    }

    public long getOrderID() {
        return orderID;
    }

    public void setOrderID(long orderID) {
        this.orderID = orderID;
    }

    public long getStationID() {
        return stationID;
    }

    public void setStationID(long stationID) {
        this.stationID = stationID;
    }

    public long getSolarsystemID() {
        return solarsystemID;
    }

    public void setSolarsystemID(long solarsystemID) {
        this.solarsystemID = solarsystemID;
    }

    public long getRegionID() {
        return regionID;
    }

    public void setRegionID(long regionID) {
        this.regionID = regionID;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public long getVolEntered() {
        return volEntered;
    }

    public void setVolEntered(long volEntered) {
        this.volEntered = volEntered;
    }

    public long getVolRemaining() {
        return volRemaining;
    }

    public void setVolRemaining(long volRemaining) {
        this.volRemaining = volRemaining;
    }

    public long getMinVolume() {
        return minVolume;
    }

    public void setMinVolume(long minVolume) {
        this.minVolume = minVolume;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public String getIssued() {
        return issued;
    }

    public void setIssued(String issued) {
        this.issued = issued;
    }

    public String getExpires() {
        return expires;
    }

    public void setExpires(String expires) {
        this.expires = expires;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }
}
