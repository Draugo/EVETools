package draugo.dev.evetools.marketdata;

import draugo.dev.evetools.Name;
import draugo.dev.evetools.Region;
import draugo.dev.evetools.marketdata.data.MarketData;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class Query {
    private static final String ITEM_ORDERS = "http://api.eve-marketdata.com/api/item_orders2.json?";

    public static MarketData buyOrders(List<Name> items, List<Region> regions) {
        if(items.isEmpty()) {
            return null;
        }
        if(regions.isEmpty()) {
            regions.add(Region.THE_FORGE);
        }
        RestTemplate restTemplate = new RestTemplate();

        String path = ITEM_ORDERS+"char_name=\"Ilessa Silvership\"&buysell=b";

        path += "&type_ids=";
        boolean first = true;
        for(Name item : items) {
            if(first) {
                first = false;
            } else {
                path += ",";
            }
            path += item.getTypeId();
        }
        path += "&region_ids=";
        first = true;
        for(Region region : regions) {
            if(first) {
                first = false;
            } else {
                path += ",";
            }
            path += region.getRegionId();
        }

        MarketData results = restTemplate.getForObject(path, MarketData.class);
        return results;
    }

    public static MarketData sellOrders(List<Name> items, List<Region> regions) {
        if(items.isEmpty()) {
            return null;
        }
        if(regions.isEmpty()) {
            regions.add(Region.THE_FORGE);
        }
        RestTemplate restTemplate = new RestTemplate();

        String path = ITEM_ORDERS+"char_name=\"Ilessa Silvership\"&buysell=s";

        path += "&type_ids=";
        boolean first = true;
        for(Name item : items) {
            if(first) {
                first = false;
            } else {
                path += ",";
            }
            path += item.getTypeId();
        }
        path += "&region_ids=";
        first = true;
        for(Region region : regions) {
            if(first) {
                first = false;
            } else {
                path += ",";
            }
            path += region.getRegionId();
        }

        MarketData results = restTemplate.getForObject(path, MarketData.class);
        return results;
    }
}
