package draugo.dev.evetools;

/**
 * General purpose immutable draugo.dev.evetools.Pair object
 */
public class Pair<T, E> {
    private final T left;
    private final E right;

    public Pair(T left, E right) {
        this.left = left;
        this.right = right;
    }

    public T getLeft() {
        return left;
    }

    public E getRight() {
        return right;
    }
}
