package draugo.dev.evetools.processes;

import draugo.dev.evetools.EVETool;
import draugo.dev.evetools.Name;
import draugo.dev.evetools.Skill;
import draugo.dev.evetools.items.Mineral;
import draugo.dev.evetools.items.Ore;
import draugo.dev.evetools.Pair;

import java.util.*;

public final class Reprocess {
    public static Map<Name, Reprocess> processes = new HashMap<>();

    static {
        processes.put(Name.VELDSPAR, new Reprocess(
                Ore.get(Name.VELDSPAR)
                , new Pair<>(Mineral.get(Name.TRITANIUM),4.15f)));
        processes.put(Name.VELDSPAR_DENSE, new Reprocess(
                Ore.get(Name.VELDSPAR_DENSE)
                , new Pair<>(Mineral.get(Name.TRITANIUM),4.57f)));
        processes.put(Name.VELDSPAR_CONCENTRATED, new Reprocess(
                Ore.get(Name.VELDSPAR_CONCENTRATED)
                , new Pair<>(Mineral.get(Name.TRITANIUM),4.36f)));

        processes.put(Name.SCORDITE, new Reprocess(
                Ore.get(Name.SCORDITE)
                , new Pair<>(Mineral.get(Name.TRITANIUM), 3.46f)
                , new Pair<>(Mineral.get(Name.PYERITE), 1.73f)));
        processes.put(Name.SCORDITE_CONDENSED, new Reprocess(
                Ore.get(Name.SCORDITE_CONDENSED)
                , new Pair<>(Mineral.get(Name.TRITANIUM), 3.63f)
                , new Pair<>(Mineral.get(Name.PYERITE), 1.82f)));
        processes.put(Name.SCORDITE_MASSIVE, new Reprocess(
                Ore.get(Name.SCORDITE_MASSIVE)
                , new Pair<>(Mineral.get(Name.TRITANIUM), 3.80f)
                , new Pair<>(Mineral.get(Name.PYERITE), 1.90f)));

        processes.put(Name.PYROXERES, new Reprocess(
                Ore.get(Name.PYROXERES)
                , new Pair<>(Mineral.get(Name.TRITANIUM), 3.51f)
                , new Pair<>(Mineral.get(Name.PYERITE), 0.25f)
                , new Pair<>(Mineral.get(Name.MEXALLON), 0.5f)
                , new Pair<>(Mineral.get(Name.NOCXIUM), 0.05f)));
        processes.put(Name.PYROXERES_SOLID, new Reprocess(
                Ore.get(Name.PYROXERES_SOLID)
                , new Pair<>(Mineral.get(Name.TRITANIUM), 3.68f)
                , new Pair<>(Mineral.get(Name.PYERITE), 0.26f)
                , new Pair<>(Mineral.get(Name.MEXALLON), 0.53f)
                , new Pair<>(Mineral.get(Name.NOCXIUM), 0.05f)));
        processes.put(Name.PYROXERES_VISCOUS, new Reprocess(
                Ore.get(Name.PYROXERES_VISCOUS)
                , new Pair<>(Mineral.get(Name.TRITANIUM), 3.85f)
                , new Pair<>(Mineral.get(Name.PYERITE), 0.27f)
                , new Pair<>(Mineral.get(Name.MEXALLON), 0.55f)
                , new Pair<>(Mineral.get(Name.NOCXIUM), 0.05f)));

        processes.put(Name.PLAGIOCLASE, new Reprocess(
                Ore.get(Name.PLAGIOCLASE)
                , new Pair<>(Mineral.get(Name.TRITANIUM), 1.07f)
                , new Pair<>(Mineral.get(Name.PYERITE), 2.13f)
                , new Pair<>(Mineral.get(Name.MEXALLON), 1.07f)));
        processes.put(Name.PLAGIOCLASE_AZURE, new Reprocess(
                Ore.get(Name.PLAGIOCLASE_AZURE)
                , new Pair<>(Mineral.get(Name.TRITANIUM), 1.12f)
                , new Pair<>(Mineral.get(Name.PYERITE), 2.24f)
                , new Pair<>(Mineral.get(Name.MEXALLON), 1.12f)));
        processes.put(Name.PLAGIOCLASE_RICH, new Reprocess(
                Ore.get(Name.PLAGIOCLASE_RICH)
                , new Pair<>(Mineral.get(Name.TRITANIUM), 1.17f)
                , new Pair<>(Mineral.get(Name.PYERITE), 2.34f)
                , new Pair<>(Mineral.get(Name.MEXALLON), 1.17f)));

        processes.put(Name.OMBER, new Reprocess(
                Ore.get(Name.OMBER)
                , new Pair<>(Mineral.get(Name.TRITANIUM), 0.85f)
                , new Pair<>(Mineral.get(Name.PYERITE), 0.34f)
                , new Pair<>(Mineral.get(Name.ISOGEN), 0.85f)));
        processes.put(Name.OMBER_SILVERY, new Reprocess(
                Ore.get(Name.OMBER_SILVERY)
                , new Pair<>(Mineral.get(Name.TRITANIUM), 0.89f)
                , new Pair<>(Mineral.get(Name.PYERITE), 0.36f)
                , new Pair<>(Mineral.get(Name.ISOGEN), 0.89f)));
        processes.put(Name.OMBER_GOLDEN, new Reprocess(
                Ore.get(Name.OMBER_GOLDEN)
                , new Pair<>(Mineral.get(Name.TRITANIUM), 0.94f)
                , new Pair<>(Mineral.get(Name.PYERITE), 0.38f)
                , new Pair<>(Mineral.get(Name.ISOGEN), 0.94f)));

        processes.put(Name.KERNITE, new Reprocess(
                Ore.get(Name.KERNITE)
                , new Pair<>(Mineral.get(Name.TRITANIUM), 1.34f)
                , new Pair<>(Mineral.get(Name.MEXALLON), 2.67f)
                , new Pair<>(Mineral.get(Name.ISOGEN), 1.34f)));
        processes.put(Name.KERNITE_FIERY, new Reprocess(
                Ore.get(Name.KERNITE_FIERY)
                , new Pair<>(Mineral.get(Name.TRITANIUM), 1.47f)
                , new Pair<>(Mineral.get(Name.MEXALLON), 2.94f)
                , new Pair<>(Mineral.get(Name.ISOGEN), 1.47f)));
        processes.put(Name.KERNITE_LUMINOUS, new Reprocess(
                Ore.get(Name.KERNITE_LUMINOUS)
                , new Pair<>(Mineral.get(Name.TRITANIUM), 1.40f)
                , new Pair<>(Mineral.get(Name.MEXALLON), 2.81f)
                , new Pair<>(Mineral.get(Name.ISOGEN), 1.40f)));

        processes.put(Name.JASPET, new Reprocess(
                Ore.get(Name.JASPET)
                , new Pair<>(Mineral.get(Name.TRITANIUM), 0.72f)
                , new Pair<>(Mineral.get(Name.PYERITE), 1.21f)
                , new Pair<>(Mineral.get(Name.MEXALLON), 1.44f)
                , new Pair<>(Mineral.get(Name.NOCXIUM), 0.72f)
                , new Pair<>(Mineral.get(Name.ZYDRINE), 0.03f)));
        processes.put(Name.JASPET_PRISTINE, new Reprocess(
                Ore.get(Name.JASPET_PRISTINE)
                , new Pair<>(Mineral.get(Name.TRITANIUM), 0.79f)
                , new Pair<>(Mineral.get(Name.PYERITE), 1.33f)
                , new Pair<>(Mineral.get(Name.MEXALLON), 1.58f)
                , new Pair<>(Mineral.get(Name.NOCXIUM), 0.79f)
                , new Pair<>(Mineral.get(Name.ZYDRINE), 0.03f)));
        processes.put(Name.JASPET_PURE, new Reprocess(
                Ore.get(Name.JASPET_PURE)
                , new Pair<>(Mineral.get(Name.TRITANIUM), 0.76f)
                , new Pair<>(Mineral.get(Name.PYERITE), 1.27f)
                , new Pair<>(Mineral.get(Name.MEXALLON), 1.51f)
                , new Pair<>(Mineral.get(Name.NOCXIUM), 0.76f)
                , new Pair<>(Mineral.get(Name.ZYDRINE), 0.03f)));

        processes.put(Name.HEMORPHITE, new Reprocess(
                Ore.get(Name.HEMORPHITE)
                , new Pair<>(Mineral.get(Name.TRITANIUM), 1.80f)
                , new Pair<>(Mineral.get(Name.PYERITE), 0.72f)
                , new Pair<>(Mineral.get(Name.MEXALLON), 0.17f)
                , new Pair<>(Mineral.get(Name.ISOGEN), 0.59f)
                , new Pair<>(Mineral.get(Name.NOCXIUM), 1.18f)
                , new Pair<>(Mineral.get(Name.ZYDRINE), 0.08f)));
        processes.put(Name.HEMORPHITE_RADIANT, new Reprocess(
                Ore.get(Name.HEMORPHITE_RADIANT)
                , new Pair<>(Mineral.get(Name.TRITANIUM), 1.98f)
                , new Pair<>(Mineral.get(Name.PYERITE), 0.79f)
                , new Pair<>(Mineral.get(Name.MEXALLON), 0.19f)
                , new Pair<>(Mineral.get(Name.ISOGEN), 0.65f)
                , new Pair<>(Mineral.get(Name.NOCXIUM), 1.29f)
                , new Pair<>(Mineral.get(Name.ZYDRINE), 0.09f)));
        processes.put(Name.HEMORPHITE_VIVID, new Reprocess(
                Ore.get(Name.HEMORPHITE_VIVID)
                , new Pair<>(Mineral.get(Name.TRITANIUM), 1.89f)
                , new Pair<>(Mineral.get(Name.PYERITE), 0.76f)
                , new Pair<>(Mineral.get(Name.MEXALLON), 0.18f)
                , new Pair<>(Mineral.get(Name.ISOGEN), 0.62f)
                , new Pair<>(Mineral.get(Name.NOCXIUM), 1.23f)
                , new Pair<>(Mineral.get(Name.ZYDRINE), 0.09f)));
    }

    private final Ore source;
    private final List<Pair<Mineral, Float>> products = new ArrayList<>();

    @SafeVarargs
    public Reprocess(Ore source, Pair<Mineral, Float> product, Pair<Mineral, Float>... products) {
        this.source = source;
        this.products.add(product);
        Collections.addAll(this.products, products);
    }

    public List<Pair<Mineral, Integer>> process(int amount) {
        // Let's normalise to batch size.
        // TODO: Account for compressed variants and add batch size to ore directly
        amount = amount - (amount%100);

        float efficiency = EVETool.BASE_YIELD
                *(EVETool.getSkill(Skill.REPROCESSING)*0.03f+1)
                *(EVETool.getSkill(Skill.REPROCESSING_EFFICIENCY)*0.02f+1)
                *(EVETool.getSkill(source.getProcessingSkill())*0.02f+1)
                *(EVETool.REPROCESS_IMPLANT+1);

        List<Pair<Mineral, Integer>> results = new ArrayList<>();
        for(Pair<Mineral, Float> process : products) {
            results.add(new Pair<>(process.getLeft(), (int)(amount*process.getRight()*efficiency)));
        }
        return results;
    }

    public void printInfo() {
        System.out.println("Ore: "+source.getName());
        System.out.println("Is processed into "+products.size()+ " mineral"+(products.size() > 1 ? "s":""));
        for(int i=0; i<products.size();i++) {
            System.out.println((i+1)+". "+products.get(i).getLeft().getName());
        }
    }
}
