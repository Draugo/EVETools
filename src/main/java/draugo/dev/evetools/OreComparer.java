package draugo.dev.evetools;

import draugo.dev.evetools.items.Mineral;
import draugo.dev.evetools.items.Ore;
import draugo.dev.evetools.processes.Reprocess;

import java.util.ArrayList;
import java.util.List;

public class OreComparer {
    private final Ore ore;
    private final int amount;
    private final Reprocess process;

    public OreComparer(Ore ore, int amount) {
        this.ore = ore;
        this.amount = amount;
        this.process = Reprocess.processes.get(ore.getName());
    }

    public void runComparison() {
        int orePrice = (int)(Prices.highestBuyPrice(ore.getName(), new ArrayList<>()) * amount);
        int mineralPrice = 0;
        System.out.println("Current price for "+amount+" units of "+ore.getName()+" is "+orePrice+" ISK");
        List<Pair<Mineral, Integer>> results = process.process(amount);
        System.out.println("Reprocessing with current skills and base efficiency of "+EVETool.BASE_YIELD+" gives:");
        for(Pair<Mineral, Integer> result : results) {
            int price = (int)(Prices.highestBuyPrice(result.getLeft().getName(), new ArrayList<>()) * result.getRight());
            System.out.println(result.getRight()+" units of "+result.getLeft().getName()+" sell for "+price+" ISK");
            mineralPrice += price;
        }
        if(orePrice > mineralPrice) {
            System.out.println("Selling the ore gives "+(orePrice-mineralPrice)+" more ISK ("+(((int)((((orePrice*1.0f)/(mineralPrice*1.0f))-1.0f)*10000))/100.0f)+"%)");
        } else if(orePrice < mineralPrice) {
            System.out.println("Selling the minerals gives "+(mineralPrice-orePrice)+" more ISK ("+(((int)((((mineralPrice*1.0f)/(orePrice*1.0f))-1.0f)*10000))/100.0f)+"%)");
        } else {
            System.out.println("Well what do you know, it doesn't matter");
        }
    }

    public void printInfo() {
        System.out.println("Comparison for "+amount+" of "+ore.getName());
        process.printInfo();
    }
}
