package draugo.dev.evetools;

import java.util.HashMap;
import java.util.Map;

public enum Skill {
    HEMORPHITE_PROCESSING("Hemorphite processing"),
    JASPET_PROCESSING("Jaspet processing"),
    KERNITE_PROCESSING("Kernite processing"),
    OMBER_PROCESSING("Omber processing"),
    PLAGIOCLASE_PROCESSING("Plagioclase processing"),
    PYROXERES_PROCESSING("Pyroxeres processing"),
    REPROCESSING("Reprocessing"),
    REPROCESSING_EFFICIENCY("Reprocessing efficiency"),
    SCORDITE_PROCESSING("Scordite processing"),
    VELDSPAR_PROCESSING("Veldspar processing");

    private final String name;

    Skill(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    private static final Map<Name, Skill> reprocessingSkills = new HashMap<>();

    static {
        reprocessingSkills.put(Name.VELDSPAR, VELDSPAR_PROCESSING);
        reprocessingSkills.put(Name.VELDSPAR_DENSE, VELDSPAR_PROCESSING);
        reprocessingSkills.put(Name.VELDSPAR_CONCENTRATED, VELDSPAR_PROCESSING);

        reprocessingSkills.put(Name.SCORDITE, SCORDITE_PROCESSING);
        reprocessingSkills.put(Name.SCORDITE_CONDENSED, SCORDITE_PROCESSING);
        reprocessingSkills.put(Name.SCORDITE_MASSIVE, SCORDITE_PROCESSING);

        reprocessingSkills.put(Name.PYROXERES, PYROXERES_PROCESSING);
        reprocessingSkills.put(Name.PYROXERES_SOLID, PYROXERES_PROCESSING);
        reprocessingSkills.put(Name.PYROXERES_VISCOUS, PYROXERES_PROCESSING);

        reprocessingSkills.put(Name.PLAGIOCLASE, PLAGIOCLASE_PROCESSING);
        reprocessingSkills.put(Name.PLAGIOCLASE_AZURE, PLAGIOCLASE_PROCESSING);
        reprocessingSkills.put(Name.PLAGIOCLASE_RICH, PLAGIOCLASE_PROCESSING);

        reprocessingSkills.put(Name.OMBER, OMBER_PROCESSING);
        reprocessingSkills.put(Name.OMBER_SILVERY, OMBER_PROCESSING);
        reprocessingSkills.put(Name.OMBER_GOLDEN, OMBER_PROCESSING);

        reprocessingSkills.put(Name.KERNITE, KERNITE_PROCESSING);
        reprocessingSkills.put(Name.KERNITE_FIERY, KERNITE_PROCESSING);
        reprocessingSkills.put(Name.KERNITE_LUMINOUS, KERNITE_PROCESSING);

        reprocessingSkills.put(Name.JASPET, JASPET_PROCESSING);
        reprocessingSkills.put(Name.JASPET_PRISTINE, JASPET_PROCESSING);
        reprocessingSkills.put(Name.JASPET_PURE, JASPET_PROCESSING);

        reprocessingSkills.put(Name.HEMORPHITE, HEMORPHITE_PROCESSING);
        reprocessingSkills.put(Name.HEMORPHITE_RADIANT, HEMORPHITE_PROCESSING);
        reprocessingSkills.put(Name.HEMORPHITE_VIVID, HEMORPHITE_PROCESSING);
    }
    public static Skill getReprocessingSkill(Name name) {
        return reprocessingSkills.get(name);
    }
}
