package draugo.dev.evetools;

public enum Region {
    ARIDIA(10000054, "Aridia"),
    BLACK_RISE(10000069, "Black Rise"),
    BRANCH(10000055, "Branch"),
    CACHE(10000007, "Cache"),
    CATCH(10000014, "Catch"),
    CLOUD_RING(10000051, "Cloud Ring"),
    COBALT_EDGE(10000053, "Cobalt Edge"),
    CURSE(10000012, "Curse"),
    DEKLEIN(10000035, "Deklein"),
    DELVE(10000060, "Delve"),
    DERELIK(10000001, "Derelik"),
    DETORID(10000005, "Detorid"),
    DEVOID(10000036, "Devoid"),
    DOMAIN(10000043, "Domain"),
    ESOTERIA(10000039, "Esoteria"),
    ESSENCE(10000064, "Essence"),
    ETHERIUM_REACH(10000027, "Etherium Reach"),
    EVERYSHORE(10000037, "Everyshore"),
    FADE(10000046, "Fade"),
    FEYTHABOLIS(10000056, "Feythabolis"),
    FOUNTAIN(10000058, "Fountain"),
    GEMINATE(10000029, "Geminate"),
    GENESIS(10000067, "Genesis"),
    GREAT_WILDLANDS(10000011, "Great Wildlands"),
    HEIMATAR(10000030, "Heimatar"),
    IMMENSEA(10000025, "Immensea"),
    IMPASS(10000031, "Impass"),
    INSMOTHER(10000009, "Insmother"),
    KADOR(10000052, "Kador"),
    KHANID(10000049, "Khanid"),
    KOR_AZOR(10000065, "Kor-Azor"),
    LONETREK(10000016, "Lonetrek"),
    MALPAIS(10000013, "Malpais"),
    METROPOLIS(10000042, "Metropolis"),
    MOLDEN_HEATH(10000028, "Molden Heath"),
    OASA(10000040, "Oasa"),
    OMIST(10000062, "Omist"),
    OUTER_PASSAGE(10000021, "Outer Passage"),
    OUTER_RING(10000057, "Outer Ring"),
    PARAGON_SOUL(10000059, "Paragon Soul"),
    PERIOD_BASIS(10000063, "Period Basis"),
    PERRIGEN_FALLS(10000066, "Perrigen Falls"),
    PLACID(10000048, "Placid"),
    PROVIDENCE(10000047, "Providence"),
    PURE_BLIND(10000023, "Pure Blind"),
    QUERIOUS(10000050, "Querious"),
    SCALDING_PASS(10000008, "Scalding Pass"),
    SING_LAISON(10000032, "Sinq Laison"),
    SOLITUDE(10000044, "Solitude"),
    STAIN(10000022, "Stain"),
    SYNDICATE(10000041, "Syndicate"),
    TASH_MURKON(10000020, "Tash-Murkon"),
    TENAL(10000045, "Tenal"),
    TENERIFIS(10000061, "Tenerifis"),
    THE_BLEAK_LANDS(10000038, "The Bleak Lands"),
    THE_CITADEL(10000033, "The Citadel"),
    THE_FORGE(10000002, "The Forge"),
    THE_KALEVALA_EXPANSE(10000034, "The Kalevala Expanse"),
    THE_SPIRE(10000018, "The Spire"),
    TRIBUTE(10000010, "Tribute"),
    VALE_OF_THE_SILENT(10000003, "Vale of the Silent"),
    VENAL(10000015, "Venal"),
    VERGE_VENDOR(10000068, "Verge Vendor"),
    WICKED_CREEK(10000006, "Wicked Creek");

    private final int regionId;
    private final String name;

    Region(int regionId, String name) {
        this.regionId = regionId;
        this.name = name;
    }

    public int getRegionId() {
        return regionId;
    }

    public String getName() {
        return name;
    }
}
