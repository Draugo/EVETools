package draugo.dev.evetools;

import draugo.dev.evetools.items.Item;

import java.util.HashMap;
import java.util.Map;

public final class Items {
    public static final Map<Name, Item> items = new HashMap<>();

    private Items() {}
}
